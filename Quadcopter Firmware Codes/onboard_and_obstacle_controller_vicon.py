#!/usr/bin/env python3

import numpy as np 
import rospy # for exception handling only
from scipy.io import savemat
from pycrazyswarm import *
from datetime import datetime

# Auxiliary functions
## NOTE: make sure to test your curve in bezier_test.ipynb first
def catmull_rom_to_bezier(p0, p1, p2, p3):
    """
    Generate control points for a cubic Bezier curve.
    The Bezier curve will trace points between p1 and p2, given **external** control points p0 and p3.
    """
    b0 = p1
    b1 = p1 + (p2 - p0) / 6 
    b2 = p2 - (p3 - p1) / 6
    b3 = p2
    return b0, b1, b2, b3

def cubic_bezier_curve(t, P0, P1, P2, P3):
    """
    Generate a cubic Bezier curve that traces P0 and P3, running smoothly near control points P1 and P2.
    """
    B0 = (1 - t)**3
    B1 = 3 * t * (1 - t)**2
    B2 = 3 * t**2 * (1 - t)
    B3 = t**3
    return P0 * B0 + P1 * B1 + P2 * B2 + P3 * B3

def generate_bezier_points(P0, P1, P2, P3, num_points):
    t_values = np.linspace(0, 1, num_points)
    points = [cubic_bezier_curve(t, P0, P1, P2, P3) for t in t_values]
    return points

if __name__ == "__main__":
    # Set up configuration
    sleepRate = 10 #Hz
    report_rate = 10 #steps

    # Enable/disable obstacle and agent operation
    obstacles_enabled = True
    agents_enabled = True
    
    agent_ids = [1,2,3,4,5] # NOTE: (agent) quadcopters excluding obstacles
    obs_ids = [6,7] # obstacle quadcopters

    quadNum = len(agent_ids) 
    obsNum = len(obs_ids) 

    target_height = 0.75 # meters, from crazyflie_psm_*.c firmware code
    takeoff_duration = 2.5 # seconds, from crazyflie_psm_*.c firmware code
    landing_height = 0.03 # meters, from crazyflie_psm_*.c firmware code 
    landing_duration = 3.0 # seconds, from crazyflie_psm_*.c firmware code 
    obstacle_navigation_iters = 140 # ticks, NOTE: this is just the best guess, modify if needed
    agent_finish_distance = 3.5 # meters, y distance travelled to terminate loop
    obstacle_finish_distance = -2.0 # meters, y distance travelled to stop obstacle navigation
    obstacle_hover_pre_delay = 1.1 # seconds from crazyflie_psm_*.c firmware code
    obstacle_hover_post_delay = 3.0 # seconds from crazyflie_psm_*.c firmware code
    landing_delay = 10.0 # seconds

    # Generate obstacle trajectories
    bezier_control_points = []
    # for each obstacle, append an array like below # NOTE: these are just examples for now, need to work for better values
    bezier_control_points.append(
        np.array([
            [-2,2],
            [0.3,0.5],
            [0.3,-1.0],
            [-1.8,-2.5]
        ])
    )

    bezier_control_points.append(
        np.array([
            [2,2],
            [-0.3,0.5],
            [-0.3,-1.0],
            [1.8,-2.5]
        ])
    )
    
    obstacle_trajectories = [np.asarray(generate_bezier_points(*bezier_control_points[i], obstacle_navigation_iters)) for i in range(len(bezier_control_points))]
    
    # Position memory arrays
    obs_pos_list_i = np.zeros((obsNum,3))
    quad_pos_list_i = np.zeros((quadNum,3))

    obs_pos_log = np.zeros((500,obsNum*3)) # position log history
    agent_pos_log = np.zeros((500,quadNum*3)) # position log history

    # Start crazyflie server
    swarm = Crazyswarm()
    timeHelper = swarm.timeHelper
    allcfs = swarm.allcfs

    # Start experiment
    try:    
        file_label = datetime.now().strftime('%d_%m_%Y_%H_%M_%S')
        if agents_enabled:
            for crazyflie_id in agent_ids:
                allcfs.crazyfliesById[crazyflie_id].setParam("fmodes/ufs",int(1))
            print("Starting experiment.")
        
        # Initialize flags
        obstacles_navigating = True
        agents_navigating = True
        obstacle_hover_state = "pre-delay"
        time_iters = 0 # NOTE: time = time_iters/sleepRate
        obs_waypoint_id = 0
        # Main loop
        while (agents_navigating and (time_iters<400)):
            # Access crazyflies
            ## Access agents
            if agents_enabled:
                for crazyflie_id in agent_ids:
                    # Check agent position
                    quad_pos_list_i[agent_ids.index(crazyflie_id)] = allcfs.crazyfliesById[crazyflie_id].position()   
            ## Access obstacles
            if obstacles_enabled:
                for crazyflie_id in obs_ids: # NOTE: obstacle indices must follow AFTER agent indices, modify if otherwise
                    # Check obstacle position
                    obs_pos_list_i[obs_ids.index(crazyflie_id)] = allcfs.crazyfliesById[crazyflie_id].position() # NOTE: can also use allcfs.crazyfliesById[id] if not ordered, returns np.array[3]

                # Obstacle finite state machine
                if obstacle_hover_state == "pre-delay":
                    obstacle_hover_state = "liftoff" if time_iters >= obstacle_hover_pre_delay*sleepRate else "pre-delay"
                if obstacle_hover_state == "liftoff":
                    for crazyflie_id in obs_ids:
                        allcfs.crazyfliesById[crazyflie_id].takeoff(target_height,takeoff_duration) 
                    obstacle_hover_state = "post-delay"
                if obstacle_hover_state == "post-delay":
                    obstacle_hover_state = "hovering" if time_iters >= (obstacle_hover_pre_delay + obstacle_hover_post_delay)*sleepRate else "post-delay"
                # 2. Obstacle waypoint navigation
                if obstacle_hover_state == "hovering": 
                    try: 
                        for crazyflie_id in obs_ids:        
                            destination = obstacle_trajectories[obs_ids.index(crazyflie_id)][obs_waypoint_id]
                            allcfs.crazyfliesById[crazyflie_id].cmdPosition(pos= [destination[0],destination[1],target_height])   
                    except IndexError:
                        for crazyflie_id in obs_ids:                    
                            print(f"Trajectory ID: {obs_ids.index(crazyflie_id)}, Waypoint: {obs_waypoint_id}")
                            destination = obstacle_trajectories[obs_ids.index(crazyflie_id)-quadNum][-1]
                            allcfs.crazyfliesById[crazyflie_id].cmdPosition(pos= [destination[0],destination[1],target_height])  

            # Pre-loop
            ## Flag checks and memory management
            agent_pos_log[time_iters,:] = quad_pos_list_i.flatten()
            obs_pos_log[time_iters,:] = obs_pos_list_i.flatten()
            agents_navigating = False if np.any(quad_pos_list_i[:,1] > agent_finish_distance) else True            
            obstacles_navigating = False if np.any(obs_pos_list_i[:,1] < obstacle_finish_distance) else True

            ## Iteration and timing
            time_iters += 1
            obs_waypoint_id = obs_waypoint_id+1 if (obstacles_navigating and obstacle_hover_state == "hovering") else obs_waypoint_id
            timeHelper.sleepForRate(sleepRate)
            if time_iters % report_rate == 0:
                print(f"Current time step: {time_iters}, Bezier step: {obs_waypoint_id}, Agents up: {agents_navigating}, Obstacles Up: {obstacles_navigating}")

        # Landing
        ## Land obstacles by teleoperation
        print("Landing sequence initiated")
        if obstacles_enabled:
            for crazyflie_id in obs_ids:
                try:
                    allcfs.crazyfliesById[crazyflie_id].notifySetpointsStop()
                    allcfs.crazyfliesById[crazyflie_id].land(targetHeight= landing_height,duration= landing_duration)  
                except rospy.service.ServiceException:
                    print(f"Obstacle {crazyflie_id} cannot land. Check mocap input.")
        ## Signal the onboard controller for the landing sequence
        if agents_enabled:
            for crazyflie_id in agent_ids:
                try:
                    allcfs.crazyfliesById[crazyflie_id].setParam("fmodes/ufs",int(4))
                except rospy.service.ServiceException:
                    print(f"Agent {crazyflie_id} cannot receive landing instructions.")
        timeHelper.sleep(landing_delay)         

        print("Ending experiment.")

        savemat(f'obs_pos_log_{file_label}.mat', {"pos":obs_pos_log})
        savemat(f'agent_pos_log_{file_label}.mat', {"pos":agent_pos_log})
    
    except Exception as e:
        print("Emergency landing initiated: {e}")
        ## Land obstacles by teleoperation
        if obstacles_enabled:
            for crazyflie_id in obs_ids:
                try:
                    allcfs.crazyfliesById[crazyflie_id].land(landing_height,landing_duration)  
                except rospy.service.ServiceException:
                    print(f"Obstacle {crazyflie_id} cannot land. Check mocap input.")   
        ## Signal the onboard controller for the landing sequence
        if agents_enabled:
            for crazyflie_id in agent_ids:
                try:
                    allcfs.crazyfliesById[crazyflie_id].setParam("fmodes/ufs",int(4))
                except rospy.service.ServiceException:
                    print(f"Agent {crazyflie_id} cannot receive landing instructions.")
        timeHelper.sleep(landing_delay)

        print("Saving files.")
        savemat(f'obs_pos_log_{file_label}.mat', {"pos":obs_pos_log})
        savemat(f'agent_pos_log_{file_label}.mat', {"pos":agent_pos_log})