#!/usr/bin/env python

import numpy as np
from scipy.io import savemat
from pycrazyswarm import *

# Auxiliary functions
## NOTE: make sure to test your curve in bezier_test.ipynb first
def catmull_rom_to_bezier(p0, p1, p2, p3):
    """
    Generate control points for a cubic Bezier curve.
    The Bezier curve will trace points between p1 and p2, given **external** control points p0 and p3.
    """
    b0 = p1
    b1 = p1 + (p2 - p0) / 6 
    b2 = p2 - (p3 - p1) / 6
    b3 = p2
    return b0, b1, b2, b3

def cubic_bezier_curve(t, P0, P1, P2, P3):
    """
    Generate a cubic Bezier curve that traces P0 and P3, running smoothly near control points P1 and P2.
    """
    B0 = (1 - t)**3
    B1 = 3 * t * (1 - t)**2
    B2 = 3 * t**2 * (1 - t)
    B3 = t**3
    return P0 * B0 + P1 * B1 + P2 * B2 + P3 * B3

def generate_bezier_points(P0, P1, P2, P3, num_points):
    t_values = np.linspace(0, 1, num_points)
    points = [cubic_bezier_curve(t, P0, P1, P2, P3) for t in t_values]
    return points

## Waypoint generation
def next_obstacle_waypoint(obstacle_position, seeking_distance, obstacle_id):
    # NOTE: this is optional if open loop solutions aren't a good idea
    # TODO: generate different trajectories for different obstacles, using Bézier curves
    # TODO: select point of seeking_distance to obstacle_position on trajectory when such a trajectory is generated
        # TODO: alternatively, use an open loop solution that generates seeking_distance spaced waypoints which yields next waypoints through iterations
    pass

if __name__ == "__main__":
    sleepRate = 10 #Hz
    
    quadNum = 6 # NOTE: (agent) quadcopters excluding obstacles
    obsNum = 3 # obstacle quadcopters

    target_height = 0.75 # meters, from crazyflie_psm_*.c firmware code
    takeoff_duration = 2.5 # seconds, from crazyflie_psm_*.c firmware code
    landing_height = 0.03 # meters, from crazyflie_psm_*.c firmware code 
    landing_duration = 3.0 # seconds, from crazyflie_psm_*.c firmware code 
    # seeking_distance = 0.05 # meters, NOTE: practically migration speed x period, modify if needed
    obstacle_navigation_iters = 140 # ticks, NOTE: this is just the best guess, modify if needed
    finish_distance = 0.0 # meters, y distance travelled to terminate loop

    
    bezier_control_points = []
    # for each obstacle, append an array like below # NOTE: these are just examples for now, need to work for better values
    bezier_control_points.append(
        np.array([
            [0,0],
            [2,1],
            [2,2],
            [4,3.5]
        ])
    )

    bezier_control_points.append(
        np.array([
            [4,0],
            [2,1],
            [2,2],
            [0,3.5]
        ])
    )

    bezier_control_points.append(
        np.array([
            [0,0],
            [0,1],
            [0,2],
            [0,3.5]
        ])
    )
    
    obstacle_trajectories = [np.asarray(generate_bezier_points(*bezier_control_points[i], obstacle_navigation_iters)) for i in range(len(bezier_control_points))]
    
    obs_pos_list_i = np.zeros((obsNum,3))

    pos_log = np.zeros((500,obsNum*3)) # position log history

    swarm = Crazyswarm()
    timeHelper = swarm.timeHelper
    allcfs = swarm.allcfs
    #timeHelper.sleep(10.0)
    # for crazyflie_id in range(quadNum, quadNum+obsNum):
    #     allcfs.crazyflies[crazyflie_id].setParam("fmodes/ufs",int(1))
    print("started")
    
    #timeHelper.sleep(20.0)
    mission_not_completed = 1
    time_iters = 0 # NOTE: time = time_iters/sleepRate
    while (mission_not_completed and (time_iters<400)):
        # Access crazyflies
        for crazyflie_id in range(quadNum, quadNum+obsNum): # NOTE: obstacle indices must follow AFTER agent indices, modify if otherwise
            # Check obstacle position
            obs_pos = allcfs.crazyflies[crazyflie_id].position() # NOTE: can also use allcfs.crazyfliesById[id] if not ordered, returns np.array[3]
            obs_pos_list_i[crazyflie_id-quadNum] = obs_pos
            pos_log[time_iters,:] = obs_pos_list_i.flatten()
            # 1. Hovering
            if obs_pos[2] < target_height:
                allcfs.crazyflies[crazyflie_id].takeoff(target_height,takeoff_duration) # NOTE: effectively the same as crtpCommanderHighLevelTakeoff() from C firmware
            
            # 2. Waypoint navigation
                
            # NOTE: this one is a closed loop solution
            # TODO: add thresholding for target destinations vs current positions
            # destination = next_obstacle_waypoint(obs_pos, seeking_distance, crazyflie_id-quadNum)
                
            destination = obstacle_trajectories[crazyflie_id-quadNum][time_iters]
            allcfs.crazyflies[crazyflie_id].cmdPosition(pos= [destination[0],destination[1],target_height])   
        # Loop management
        time_iters += 1
        if np.any(obs_pos_list_i[:,1] < finish_distance):
            mission_not_completed = 0
        timeHelper.sleepForRate(sleepRate)
    # 3. Landing
    for crazyflie_id in range(quadNum, quadNum+obsNum):
        allcfs.crazyflies[crazyflie_id].land(landing_height, landing_duration)      
    # allcfs.setParam("fmodes/ufs",int(4))
    print("ended")

    savemat('obs_pos_log.mat', {"pos":pos_log})
    timeHelper.sleep(10.0)
