#!/usr/bin/env python

import numpy as np
import scipy.io

from pycrazyswarm import *

if __name__ == "__main__":


    sleepRate = 10 #Hz

    quadNum = 6 # NOTE: (agent) quadcopters excluding obstacles
    obsNum = 3 # obstacle quadcopters
    cflieNum = quadNum + obsNum

    quad_pos_list_i = np.zeros((cflieNum,3))

    pos_log = np.zeros((500,cflieNum*3)) #position log history

    swarm = Crazyswarm()
    timeHelper = swarm.timeHelper
    allcfs = swarm.allcfs
    #timeHelper.sleep(10.0)
    allcfs.setParam("fmodes/ufs",int(1))
    print("started")

    #timeHelper.sleep(20.0)
    mission_not_completed = 1
    time_iters = 0 # NOTE: time = time_iters/sleepRate
    while (mission_not_completed and (time_iters<400)):
        for crazyflie_id in range(cflieNum):
            quad_pos_list_i[crazyflie_id] = allcfs.crazyflies[crazyflie_id].position()

        pos_log[time_iters,:] = quad_pos_list_i.flatten()
        time_iters = time_iters + 1
        if np.any(quad_pos_list_i[:,1] > 3.5):
            mission_not_completed = 0

        timeHelper.sleepForRate(sleepRate)

    allcfs.setParam("fmodes/ufs",int(4))
    print("ended")

    scipy.io.savemat('pos_log.mat', {"pos":pos_log})

    timeHelper.sleep(10.0)
