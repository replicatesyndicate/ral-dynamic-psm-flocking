# TODO
## Set up experiment scenario
- 6 agent robots
- 2(+1) obstacle robots
  Two options:
  - Obstacle motion opposite to incoming robots
  - Obstacle motion is diagonal to robots
  - Obstacles criscross diagonally from both sides
  - Edge case: criscross + opposite robot (3 robots)
## Set up simulation
  ### Required files
  - Sim and exp files in `Predictive Search Flocking (Dynamic)` and `RAL_Zenodo` folders
  - Main folder of interest: `RAL_Zenodo/Simulations/Single Step Prediction Flocking`
  ### Simulation files
  - Only add `RAL_Zenodo/Simulations/Single Step Prediction Flocking` to your PATH
  - Then, run `single_step_predictive_search_method.m` to run simulator
  - Can view results with `simulator.m`
  - Can get plots by running sections on `flocking_metric_plots.m`

  ### Imports from other sources
  - `Predictive Search Flocking (Dynamic)` -- red dashed lines in simulation are Beziér curves of incoming drone obstacles
## Set up real experiments
 - Obstacles are to be given Beziér trajectories
 - Define obstacle spheres as positions surrounding obstacle drones in flock drones
 - Rest is the same
## Clean up Zenodo repository
  - Add new PSM repo to the main Zenodo repo
