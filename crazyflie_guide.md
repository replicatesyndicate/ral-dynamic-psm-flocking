# Terminal commands
## cfclient
Opens the Crazyflie client
### How to use
1. Enter radio interface from top left
2. Enter crazyflie address in hexadecimal, and press scan to connect to crazyflies on given radio interface
3. Connect > Configure 2.X, write address info, etc. to change above configuration (note: the channel info must match with given config file for crazyswarm)
4. Note that the radio interface shown will contain the crazyflie address (can be checked to see matches), and the radio channel (i.e. 0/90 for ch. 90)
5. A good practice is to keep robot addresses in id order, sequentially
## crazyflie-firmware
1. Navigate to `examples/<project-name>/src` -- currently `app_vicon_RAL` (can copy/paste projects to make things easy)
2. Build by `tb make_app examples/<project-name> clean` then `tb make_app examples/<project-name> j8` (bitcraze.io / app layer api from crazyflie-firmware)
3. Navigate to the `<project-name>` folder, and call `make cload` preferably while the crazyflie unit is close to the radio to deploy
## crazyswarm package -- use crazyswarm_romer
- The `pycrazyswarm` Python package is a wrapper used to convert Python-like code to rospy
### ros_ws (ROS workspace)
1. Modify your codes to scale to the number of robots
2. `roslaunch crazyswarm hover_swarm.launch` or any other related launchfile
3. You can run `crazyswarm/scripts/chooser.py` to reconfigure the `crazyswarm.yaml` file to be able to connect to your crazyflies
4. Finally, call the appropriate Python script to control your crazyflies (e.g. `src/crazyswarm/scripts/onboard_RAL.py`)
# Hardware tests
1. Show the crazyflie to the Vicon cameras, watch the screen
2. Turn on the crazyflie unit and put it on its designated spot on the floor
3. Call your script
